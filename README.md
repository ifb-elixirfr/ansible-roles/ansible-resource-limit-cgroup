This role is intended to enable strict resource limits by using cgroups.
To do so, the following methods can be used :

Systemd :
Systemd has built-in features to group resources allocated to a user in "slices".
While one can define e.g. a default amount of CPU or RAM to limit use per user,
ATM it is not natively possible to set different values for user A and B.
As a workaround, it is possible to use a pam_exec call to change systemd settings
on the fly at user login, provided A) systemd correctly creates a session and puts
that session in the correct user slice and B)the processes are put in the correct
cgroups (I observed that under various contitions this is not the case). Therefore
this method is considered hackish for the time being.

pam_cgroup:
The PAM module using libcgroup can automatically move processes into cgroups at
login time depending on user, uid or group. This mimics the systemd slice feature
with the added benefit that complex rules can be applied using a comprehensive
configuration format. Listed groups can be templated so that every user that logs
on has its own group, however it may happen that the group does not exist before
the module wants to move processes into it. A pam_exec call is used to work this
around. Unfortunately it does not seem to be possible to make the module play
nice with systemd (by using systemd's slice-named cgroups, because it
seems systemd closely monitors their settings).

pam_slurm_adopt:
This PAM module is intended to be used in a SLURM HPC cluster when it is
configured to use cgroups. It allows to account for resources used by the logged
in user along with their allocated jobs on the compute node. It is advised to
disable pam_systemd when this module is used.

Role usage:
When using libpam_cgroup (the default), it is advised to provide a template in your
play templates directory for the files which will contain the limitation rules
for each cgroup and the classification rules to put processes in cgroups. Otherwise
the default role templates will be used. You can use them as an example to craft
suitable templates for your site.
