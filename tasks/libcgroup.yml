---
- name: Make sure libpam-cgroup is installed
  package:
    name: "{{ pam_cgroup_packages }}"
    state: present
  when: libpam_cgroup

- name: Provide libpam configuration
  template:
    src: "{{lookup('first_found', limits_rules)}}"
    dest: "{{ pam_cgroups_profiles_conffile }}"
    owner: root
    group: root
    mode: 0644
  vars:
    limits_rules:
      files:
      - "libpam_cgconfig.j2"
      - "libpam_cgconfig_default.j2"
      paths:
      - "templates"
   notify: reload cgroup rules
  when: libpam_cgroup

- name: Provide libpam configuration
  template:
    src: "{{lookup('first_found', classification_rules)}}"
    dest: /etc/cgrules.conf
    owner: root
    group: root
    mode: 0644
  vars:
    classification_rules:
      files:
      - "libpam_cgrules.j2"
      - "libpam_cgrules_default.j2"
      paths:
      - "templates"
  notify: reload cgroup rules
  when: libpam_cgroup

#May not work beacause pam-auth-update can generate a prompt
#To use this on Debian (which seems cleaner) one must make sure no deployment script makes direct changes to etc/pam.d files
- name: Use libpam_cgroup in PAM
  template:
    src: pam-configs-cgroups.j2
    dest: /usr/share/pam-configs/cgroups
    owner: root
    group: root
    mode: 0644
  when: no
  #libpam_cgroup
  notify: regenerate PAM entries

- name: Enable PAM cgroup session classifier
  lineinfile: 
    path: "/etc/pam.d/{{ item }}"
    state: "{{'present' if libpam_cgroup else 'absent'}}"
    #by default the line will be inserted at the end of the file if insertafter is not found
    insertafter: '^#?-?session.*pam_systemd.so'
    line: 'session optional        pam_cgroup.so debug'
    regexp: '^session\s*optional\s*pam_cgroup.so (debug)?'
  loop: "{{ pam_cgroup_pam_session_files }}"

- name: Addon to pre-generate cgroups on login
  template:
    src: cgroup-generator-script.j2
    dest: "{{ pam_cgroups_create_onthefly }}"
    owner: root
    group: root
    mode: 0755
  when: libpam_cgroup

- name: Addon to pre-generate cgroups on login
  file:
    path: "{{ pam_cgroups_create_onthefly }}"
    state: absent
  when: not libpam_cgroup

- name: Enable PAM cgroup generator
  #become: yes 
  lineinfile:
    path: "/etc/pam.d/{{ item }}"
    state: "{{'present' if libpam_cgroup else 'absent'}}"
    insertbefore: '^#?session.*pam_cgroup.so'
    line: 'session optional        pam_exec.so debug type=open_session {{ pam_cgroups_create_onthefly }}'
    regexp: '^session\s*optional\s*pam_exec.so.* {{ pam_cgroups_create_onthefly }}'
  when: libpam_cgroup
  loop: "{{ pam_cgroup_pam_session_files }}"

- name: Classifier daemon service file
  template:
    src: cgred.service.j2
    dest: /etc/systemd/system/cgred.service
    owner: root
    group: root
    mode: 0644
  when: libpam_cgroup and ansible_os_family == 'RedHat'

- name: Classifier daemon service file
  file:
    path: /etc/systemd/system/cgred.service
    state: absent
  when: not libpam_cgroup


- name: "{{ 'Disable' if libpam_cgroup_disable_systemd else 'Enable' }} systemd for session classification"
  replace:
    path: "/etc/pam.d/{{ item }}"
    replace: "{{ '#' if libpam_cgroup_disable_systemd }}\\1session optional        pam_systemd.so debug"
    regexp: '^#?(-?)session\s*optional\s*pam_systemd.so( debug)?'
  loop: "{{ pam_cgroup_pam_systemd_files }}"

- name: systemd cgroup profiles loader
  template:
    src: cgrules_load.service.j2
    dest: /etc/systemd/system/cgrules.service
    owner: root
    group: root
    mode: 0644
  when: libpam_cgroup

- name: Addon to pre-generate cgroups on login
  file:
    path: "/etc/systemd/system/cgrules.service"
    state: absent
  when: not libpam_cgroup

- name: Make sure rules get loaded, even at reboot
  systemd:
    name: cgrules.service
    enabled: yes
    daemon_reload: yes
  when: libpam_cgroup

- name: Use classifier daemon
  systemd:
    name: cgrules.service
    enabled: yes
    state: started
    daemon_reload: yes
  when: libpam_cgroup and ansible_os_family == 'RedHat'


